
-- SUMMARY --

The Allowed Values Element module provides a form element for which you can define an array of allowed values - the user completing the form has to enter one of these values for the element to validate.

If javascript is enabled, validation will be performed with ajax, otherwise it'll be validated on form submission.

Possible uses for this element include entering a barcode for a competition.

  http://drupal.org/project/issues/av_element
  

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- USAGE --

To create an allowed values element:

$element = array(
  '#type' => 'allowed_values',
  '#values' => array('One', 'Two'),
  '#case_insensitive' => TRUE
);

#case_insensitive defaults to false - ff you want 'one' to match 'One' set #case_insensitive to true.

-- CONTACT --

Current maintainers:
* Ben Scott (ben.scott) - http://drupal.org/user/149339
  Brighton Digital Ltd.

This project has been sponsored by:
* Comic Relief
