<?php

/**
 * @file
 * A basic form containing an allowed values element for testing
 */

function av_element_test_form() {
  
  $form['title'] = array(
    '#type' => 'allowed_values',
    '#title' => t('Allowed values test'),
    '#values' => array('One', 'Two'),
    '#case_insensitive' => TRUE
  );

  return $form;
  
}