
Drupal.behaviors.avBehavior = function (context) {

   $('input.allowed-values', context).blur(function(){
     
     avValidate($(this), $(this).attr('name'))
     
   });
  
}

function avValidate($input, name){
      
      var $parentForm = $input.parents('form');
      var $parentSpan = $input.parents('span.av');
      
      if($input.val().length){
      
        $input.addClass('allowed-values-active');
      
        data = {
          build_id : $('input[name="form_build_id"]', $parentForm).val(),
          form_id : $('input[name="form_id"]', $parentForm).val(),
          value : $input.val(),
          element_name : name,
          url: window.location.href   
        }
        
        $.ajax({
           type: "POST",
           url: "/av_element/js",
           data: data,
           dataType: 'json',
           success: function(result){
           
            $input.removeClass('allowed-values-active');
           
             if(!result.valid){
             
               if(!$parentSpan.siblings('div.allowed-values-error').length){
                            
                $input.addClass('error');
                $parentSpan.removeClass('av-validated');
                
                if(result.message){
                  $parentSpan.after(result.message);
                }
                
               }
             
             }else{
             
              $parentSpan.siblings('div.allowed-values-error').remove();
              $input.removeClass('error');
              $parentSpan.addClass('av-validated');
             
             }
           
           }
         });
         
      }
  
}
